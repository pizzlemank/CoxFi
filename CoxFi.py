# Written by Tadashi.
# Script I use to get free CoxWiFi.
# I use this on my Ubuntu with Cinnamon workstation.
# You are better off making your own macro for your own use case.
# Feel free to cross reference this with your own script.

import schedule
import time
import pyautogui as auto

print("Script initialized.\n")

def enter():
    auto.press('enter')

def macro():

    # Turn off WiFi, I would use a macro but 2lazy5me
    auto.moveTo(0, 924, duration=0.2)
    auto.click()
    time.sleep(0.5)
    auto.press('tab')
    auto.press('tab')
    enter()
    time.sleep(0.5)
    auto.click(x=1, y=924)
    time.sleep(0.2)
    auto.moveTo(30, 925)
    
    # Console MAC rotation
    auto.hotkey('ctrl', 'alt', 't')
    time.sleep(0.5)
    auto.hotkey('alt', 'z') # Shortcut I use to turn off my console's menu bar. Purely for aesthetics lol
    auto.typewrite("source .zshrc")
    enter()
    auto.typewrite("nothomelol") # zsh script that rotates my MAC using macchanger
    enter()
    auto.typewrite("") # Typically where my password would go
    enter()
    time.sleep(1)
    auto.hotkey('alt', 'f4')
    print("MAC address rotated.\n")
    time.sleep(1)
    
    # Turn WiFi back on.
    auto.moveTo(0, 924, duration=0.15)
    time.sleep(0.2)
    auto.click(x=5, y=924)
    time.sleep(0.5)
    auto.press('tab')
    auto.press('tab')
    enter()
    time.sleep(1)
    auto.click(x=5, y=924)
    auto.moveTo(30, 925)
    time.sleep(1)
    
    # Open Firefox for CoxWiFi login.
    auto.press('win')
    time.sleep(1)
    auto.typewrite("fir")
    enter()
    time.sleep(6)
    auto.typewrite("192.168.0.1") # Redirects to CoxWiFi login portal.
    enter()
    time.sleep(45)
    print("CoxWiFi login screen should be open...\n")
    
    # Fullscreen onto the login page to quickly log in.
    auto.press('f11')
    time.sleep(1)
    auto.press('tab')
    auto.press('tab')
    enter()
    time.sleep(5)
    # Spam credentials.
    auto.press('tab')
    auto.typewrite('l')
    auto.press('tab')
    auto.typewrite('o')
    auto.press('tab')
    auto.typewrite('o') # Selects 'Other' for home service.
    auto.press('tab')
    auto.typewrite('l@....') # CoxWiFi hotspots see emails with .... for the domain as valid.
    # Finished!
    auto.click(x=680, y=436) # Couldn't find a way to select the checkmark on the page without a mouse.
    time.sleep(0.5)
    auto.click(x=1193, y=469)
    time.sleep(7)
    auto.hotkey('alt', 'f4')

    print("LOL DID IT WORK?!\n") # Breaks sometimes if the connection is poor.

macro()
schedule.every(58).minutes.do(macro) # Hotspot only allows an hour per device a.k.a. unique MAC.

while 1:
    schedule.run_pending()
    time.sleep(1)

# Tutorials used:
# https://pyautogui.readthedocs.io/en/latest/quickstart.html
# https://vid.puffyan.us/watch?v=zwIGxcDxS5o
# https://vid.puffyan.us/watch?v=g00Wz5QTsOM
